fn main() {
    println!("Hello, world!");

    let v: Vec<i32> = vec![2, 3, 2, 1, 1];

    let min_sub = find_min_sum(&v, true);
    let max_sub = find_min_sum(&v, false);

    let mut min_sum: i32 = 0;
    let mut max_sum: i32 = 0;

    for min in min_sub.iter() {
        min_sum += *min;
    }

    for max in max_sub.iter() {
        max_sum += *max;
    }

    println!("{:?}", min_sum);
    println!("{:?}", max_sum);
}

/// It takes an array of numbers with size of 5 and returns an array of size 4 with minimum numbers
/// # Strategies
///     1. Loop through the array 5 times
///     2. For each loop, find minimum number by iterating each number
///     3. Remove that minimum number from the array
///     4. Repeat #2
fn find_min_sum(arr: &Vec<i32>, is_min: bool) -> Vec<i32> {
    println!(
        "starting to find {}",
        if is_min {
            "minimum sub array.\n"
        } else {
            "maxinum sub array.\n"
        }
    );
    let mut copied = arr.to_vec();
    let mut result: Vec<i32> = Vec::new();

    for _ in 0..arr.len() {
        let mut min_max_value: i32 = copied[0];
        let mut min_max_index: usize = 0;

        for j in 1..copied.len() {
            if is_min && copied[j] < min_max_value {
                min_max_value = copied[j];
                min_max_index = j;
            } else if !is_min && copied[j] > min_max_value {
                min_max_value = copied[j];
                min_max_index = j;
            }
        }

        println!("pushing {} into {:?}...", min_max_value, result);
        result.push(min_max_value);

        if result.len() == 4 {
            println!("breaking loop with result: {:?}", result);

            break;
        }

        println!(
            "removing {} at {} from {:?}...",
            min_max_value, min_max_index, copied
        );
        copied.remove(min_max_index);
    }

    println!("finished. \n");

    return result;
}
