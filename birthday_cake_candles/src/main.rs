fn main() {
    let args: Vec<String> = std::env::args().collect();

    // there must be 2 arguments excluding the first argument which is the exe name
    if args[1..].len() < 2 {
        println!("Command line arguments are invalid, {:?}", &args[1..]);
        std::process::exit(1);
    }

    let mut arr: Vec<i32> = Vec::new();

    for i in 1..args.len() {
        match args[i].parse::<i32>() {
            Ok(num) => arr.push(num),
            Err(err) => {
                println!("{:?}", err);
                std::process::exit(1);
            }
        }
    }

    let age = arr[0];

    if age as usize != arr[1..].len() {
        println!(
            "The number of items {:?} in the array must match with the age {:?}.",
            age,
            arr[1..].len()
        );
        std::process::exit(1);
    }
    let mut candle_map: std::collections::HashMap<i32, i32> = std::collections::HashMap::new();

    for candle in &arr[1..] {
        if !candle_map.contains_key(candle) {
            candle_map.insert(*candle, 1);
        } else {
            candle_map.insert(*candle, candle_map[candle] + 1);
        }
    }

    let mut highest_candle: i32 = 0;
    let mut highest_candle_count: i32 = 0;

    for (candle, count) in &candle_map {
        if *candle > highest_candle {
            highest_candle = *candle;
            highest_candle_count = candle_map[candle];
        }
    }

    println!(
        "The niece will blow {} of the candle {}.",
        highest_candle_count, highest_candle
    );
}
