fn main() {
    let grades: Vec<u8> = vec![73, 67, 38, 33];

    grading_students(grades);
}

fn grading_students(grades: Vec<u8>) {
    for grade in grades {
        let final_grade: u8 = round(grade);

        println!("{}", final_grade);
    }
}

/// This functio takes a number (grade) and returns a number (rounded grade) based on the rule.
/// So the rule is following:
///     - the difference between the grade and the next multiple of 5 is less than 3, round the grade up to the next multiple of 5.
///     - the grade is less than 38, do not round the grade.
fn round(grade: u8) -> u8 {
    let next_multiple_of_5: u8 = find_next_multiple_of_5(grade);

    let diff = next_multiple_of_5 - grade;

    if diff < 3 {
        if next_multiple_of_5 < 40 {
            return grade; // failing grade
        }

        return next_multiple_of_5;
    }

    return grade;
}

fn find_next_multiple_of_5(grade: u8) -> u8 {
    let mut next_multiple_of_5: u8 = 5;

    while next_multiple_of_5 < grade {
        next_multiple_of_5 = next_multiple_of_5 + 5;
    }

    return next_multiple_of_5;
}
