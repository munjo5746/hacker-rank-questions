fn kangaroo(x1: i32, v1: i32, x2: i32, v2: i32) -> String {
    // returns YES or NO
    // return trivial case
    if x1 == x2 {
        return String::from("YES");
    }

    // return impossible cases
    if x1 < x2 && v1 <= v2 || x2 < x1 && v2 <= v1 || v1 == v2 && x1 != x2 {
        return String::from("NO");
    }

    let mut limit = 100;

    let mut current_x1 = x1;
    let mut current_x2 = x2;
    let is_x1_faster = v1 > v2;
    loop {
        current_x1 = current_x1 + v1;
        current_x2 = current_x2 + v2;

        limit = limit - 1;
        if limit == 0 {
            break;
        }

        if current_x1 == current_x2 {
            return String::from("YES");
        }

        if is_x1_faster && current_x1 > current_x2 || !is_x1_faster && current_x2 > current_x1 {
            break;
        }
    }

    return String::from("NO");
}

fn main() {
    let result = kangaroo(2, 1, 1, 2);
    println!("{}", result);
}
