fn count_apples_and_oranges(
    s: i32,
    t: i32,
    a: i32,
    b: i32,
    apples: Vec<i32>,
    oranges: Vec<i32>,
) -> (i32, i32) {
    let distances_of_apples = traveled_distance_from_tree(a, &apples);
    let distances_of_oranges = traveled_distance_from_tree(b, &oranges);

    println!("{:?}", distances_of_apples);
    println!("{:?}", distances_of_oranges);

    let apples_in_range = find_fruits_in_range(s, t, &distances_of_apples);
    let oranges_in_range = find_fruits_in_range(s, t, &distances_of_oranges);

    return (apples_in_range, oranges_in_range);
}

/// takes tree location, t_loc and all distances of fruites that felt, fruit_distances
/// and returns the distances each fruit traveled from its tree.
fn traveled_distance_from_tree(t_loc: i32, fruit_distances: &Vec<i32>) -> Vec<i32> {
    let mut distances: Vec<i32> = Vec::new();

    for distance in fruit_distances {
        distances.push(t_loc + distance);
    }

    return distances;
}

fn find_fruits_in_range(s: i32, t: i32, distances_of_fruits: &Vec<i32>) -> i32 {
    let mut count: i32 = 0;

    for distance in distances_of_fruits {
        if *distance >= s && *distance <= t {
            println!("{} {} {}", *distance, s, t);
            count += 1;
        }
    }

    return count;
}

fn main() {
    let result = count_apples_and_oranges(7, 10, 4, 12, vec![2, 3, -4], vec![3, -2, -4]);

    println!("{:?}", result);
}
